package lt.projects.service;

import lt.projects.domain.Greeting;
import lt.projects.dto.GreetingDTO;
import lt.projects.repository.GreetingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Validated
public class GreetingService {

    @Autowired
    private GreetingRepository greetingRepository;
    @Transactional(readOnly=true)
    public List<GreetingDTO> getGreetings(){
        List<Greeting> greetings = greetingRepository.findAll();
//        for greeting in greetings {
//            greetingDate = new GreetingDateDTO();
//            greetingDate.setId(greeting.getId())
//            greetingDate.setDate(greeting.getDate())
//        }
        List<GreetingDTO> greetingList = greetings.stream().map(greeting -> new GreetingDTO(greeting)).collect(Collectors.toList());
        return greetingList;
    }

    @Transactional(readOnly=true)
    public GreetingDTO getGreeting(Long id) {
        Greeting greeting = greetingRepository.getOne(id);
        return new GreetingDTO(greeting);
    }

    @Transactional
    public void addGreeting(GreetingDTO greetingDto){
        Greeting greeting = new Greeting();

        greeting.setFirstName(greetingDto.getFirstName());
        greeting.setLastName(greetingDto.getLastName());
        greeting.setAudioUrl(greetingDto.getAudioUrl());
        greeting.setImage(greetingDto.getImage());
        greeting.setDate(greetingDto.getDate());
        greeting.setTime(greetingDto.getDate());
        greeting.setText(greetingDto.getText());

        greetingRepository.save(greeting);
    }

    @Transactional
    public void updateGreeting(Long id, GreetingDTO greetingDto){
        Greeting greeting = greetingRepository.getOne(id);

        greeting.setFirstName(greetingDto.getFirstName());
        greeting.setLastName(greetingDto.getLastName());
        greeting.setAudioUrl(greetingDto.getAudioUrl());
        greeting.setImage(greetingDto.getImage());
        greeting.setDate(greetingDto.getDate());
        greeting.setTime(greetingDto.getDate());
        greeting.setText(greetingDto.getText());

        greetingRepository.save(greeting);
    }

    @Transactional
    public void removeGreeting(Long id) {
        greetingRepository.deleteById(id);
    }

//    public void addGreeting(GreetingDTO greetingDTO) {
//        greetingRepository.addGreeting(greetingDTO);
//    }

//    public List<GreetingDTO> getGreetings() {
//        return greetingRepository.getGreetings();
//    }

    public void setGreetingRepository(GreetingRepository greetingRepository) {
        this.greetingRepository = greetingRepository;
    }

//    public void deleteGreeting(Long id) {
//        greetingRepository.deleteGreeting(id);
//    }
//
//    public void updateGreeting(Long id) {
//        greetingRepository.updateGreeting(id);
//    }

    }

