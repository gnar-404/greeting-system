package lt.projects.exception;

import java.util.Date;

public class GreetingErrorDetails {
    private Date timestamp;
    private String firstName;
    private String lastName;

    public GreetingErrorDetails() {
    }

    public GreetingErrorDetails(Date timestamp, String firstName, String lastName) {
        this.timestamp = timestamp;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
