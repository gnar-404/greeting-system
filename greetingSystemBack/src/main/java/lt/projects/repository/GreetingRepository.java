package lt.projects.repository;

import lt.projects.domain.Greeting;
import lt.projects.dto.GreetingDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public interface GreetingRepository extends JpaRepository<Greeting,Long> {

//    @Autowired
//    private EntityManager entityManager;



//    @Transactional
//    public void addGreeting(GreetingDTO greetingDTO){
//        Greeting greeting = new Greeting();
//        greeting.setFirstName(greetingDTO.getFirstName());
//        greeting.setLastName(greetingDTO.getLastName());
//        greeting.setAudioUrl(greetingDTO.getAudioUrl());
//        greeting.setImage(greeting.getImage());
//        greeting.setDate(greeting.getDate());
//        greeting.setTime(greeting.getDate());
//
//        entityManager.persist(greeting);
//    }

//    @Transactional(readOnly =true)
//    public List<GreetingDTO> getGreetings(){
//        Query query = entityManager.createQuery("select e from GREETING e");
//        List<Greeting> greetings = query.getResultList();
//        List<GreetingDTO> greetingsDTO = greetings.stream().map(greeting -> new GreetingDTO(greeting)).collect(Collectors.toList());
//        return greetingsDTO;
//    }

//    public void setEntityManager(EntityManager entityManager) {
//        this.entityManager = entityManager;
//    }
//
//    @Transactional
//    public void deleteGreeting(Long id) {
//        Greeting greeting = entityManager.createQuery("SELECT e from Greeting e where id = :id", Greeting.class)
//                .setParameter("id", id).getSingleResult();
//        if (entityManager.contains(greeting)){
//            entityManager.remove(greeting);
//        } else {
//            entityManager.remove(entityManager.merge(greeting));
//        }
//    }
//
//    public void updateGreeting(Long id) {
//    }


//    @Transactional
//    public void updateGreeting(Long id) {
//        Greeting greeting = query
//        entityManager.persist(manoNaujosReiksmes);
//        Greeting greeting = entityManager.createQuery("update e from Greeting e where id = :id", Greeting.class)
//                .setParameter(greeting).executeUpdate();
//    }
}
