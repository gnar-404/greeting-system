package lt.projects.dto;

import lt.projects.domain.Greeting;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;


public class GreetingDTO {
    @NotNull
    private Long id;

    private String image;

    @NotNull
    @Length(min = 1, max = 10)
    private String firstName;

    @NotNull
    @Length(min = 1, max = 10)
    private String lastName;

    private String text;

    private String audioUrl;

    private String date;

    private String time;

    private String type;


    public GreetingDTO() {
    }

    public GreetingDTO(Greeting greeting) {
        this.id = greeting.getId();
        this.image = greeting.getImage();
        this.firstName = greeting.getFirstName();
        this.lastName = greeting.getLastName();
        this.text = greeting.getText();
        this.audioUrl = greeting.getAudioUrl();
        this.date = greeting.getDate();
        this.time = greeting.getTime();
        this.type = greeting.getType();
    }

    public String getImage() {
        return image;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getText() {
        return text;
    }

    public String getAudioUrl() {
        return audioUrl;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getType() {
        return type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setAudioUrl(String audioUrl) {
        this.audioUrl = audioUrl;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setType(String type) {
        this.type = type;
    }
}
