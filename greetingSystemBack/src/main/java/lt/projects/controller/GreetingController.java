package lt.projects.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lt.projects.dto.GreetingDTO;
import lt.projects.exception.ExceptionController;
import lt.projects.exception.GreetingErrorDetails;
import lt.projects.service.GreetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.util.Date;
import java.util.List;

@Api(value = "greeting")
@RestController
@ControllerAdvice
public class GreetingController extends ExceptionController {

    @Autowired
    private GreetingService greetingService;

    @RequestMapping(method = RequestMethod.GET, value = "/api/greetings")
    @ApiOperation(value="Get greetings",notes ="Returns greetings")
    @ResponseStatus(HttpStatus.OK)
    public List<GreetingDTO> getGreetings(){
        return greetingService.getGreetings();
    }

    @ApiOperation(value = "Get greeting by id", notes="Returns a single greeting by id")
    @RequestMapping(path="/api/greetings/{id}",method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public GreetingDTO getGreeting(@PathVariable final Long id){
        return greetingService.getGreeting(id);
    }



    @ApiOperation(value = "add greeting", notes = "Adds greeting")
    @RequestMapping(value = "/api/greetings",method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void addGreeting(
            @ApiParam(value = "", required = true)
            @Valid
            @RequestBody GreetingDTO greeting){
        greetingService.addGreeting(greeting);
    }



    @ApiOperation(value = "delete greeting", notes = "Delete greeting by id")
    @RequestMapping(value = "/api/greetings/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void deleteGreeting(@ApiParam(value = "", required = true)@PathVariable final Long id){
        greetingService.removeGreeting(id);
    }

    @ApiOperation(value = "update Greeting", notes = "Uptades Greeting by id")
    @RequestMapping(value = "/api/greetings/{id}",method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public void updateGreeting(
            @ApiParam(value = "", required = true)
            @PathVariable Long id,
            @RequestBody GreetingDTO greetingDto
    ){
        greetingService.updateGreeting(id, greetingDto);
    }

    public void setGreetingService(GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    @RequestMapping(path="/test", method = RequestMethod.GET)
    String test() {
        return "Testing...";
    }
}
