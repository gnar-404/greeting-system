package lt.projects;


import static org.hamcrest.CoreMatchers.is;

import lt.projects.domain.Greeting;
import lt.projects.dto.GreetingDTO;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = App.class)
public class GreetingControllerTest {

    private static final String URI = "/api/greetings";
//    Sita klase Leidzia ivykdyti java koda per rest uzklausas
    @Autowired
    private TestRestTemplate rest;


    @Test
    public void createsGreetingThenRetrievesGreetingListAndDeletesGreeting() {
    final String greetingName = "test";
    final GreetingDTO greeting = new GreetingDTO();
        greeting.setFirstName(greetingName);
        greeting.setLastName("penis");

        greeting.setId(1L);

        rest.postForLocation(URI, greeting);

        List<Greeting> greetings = rest.getForEntity(URI, List.class).getBody();

        MatcherAssert.assertThat(greetings.size(), CoreMatchers.is(1));

        rest.delete(URI + "/" + greeting.getId());
        greetings = rest.getForEntity(URI, List.class).getBody();
        MatcherAssert.assertThat(greetings.size(), CoreMatchers.is(0));
    }




//    @Value("${local.server.port}")
//    int port;
//
//    @Before
//    public void setUp() throws Exception {
//        RestAssured.port = port;
//    }
//
//    @Test
//    public void test() throws Exception {
//        when().get("/api/greetings/test").then().body(is("Testing..."));
//    }

}
