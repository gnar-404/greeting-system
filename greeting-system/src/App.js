import React from "react";
import { Route, Switch } from "react-router-dom";

import Navigation from "./components/Navigation/NavigationComponent";
import NoMatch from "./components/NoMatch/NoMatchComponent";

import GreetingsListContainer from "./components/GreetingsList/GreetingsListContainer";
// import GreetingsDetailsContainer from "./components/GreetingsDetails/GreetingsDetailsContainer";

import GreetingsAdminFormContainer from "./components/GreetingsAdmin/GreetingsAdminFromContainer";

// import GreetingsAdminTableContainer from "./components/GreetingsAdmin/GreetingsAdminTableContainer";

import { Routes } from "./components/constants/routes";

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

const App = (props) => (
  <div className="App">
    <Navigation />
    <Switch>
      <Route exact path={Routes.all_greetings} component={GreetingsListContainer} />
      <Route path={Routes.new_greeting} component={GreetingsAdminFormContainer} />

      {/* <Route path={Routes.greeting_id} component={GreetingsDetailsContainer} />
      <Route path={Routes.all_greetings} component={GreetingsListContainer} /> */}

      {/* <Route path={Routes.admin_greetings_new} component={GreetingsAdminFormContainer} />
      <Route path={Routes.admin_greetings_id} component={GreetingsAdminFormContainer} /> */}

      {/* <Route path={Routes.admin_greetings} component={GreetingsAdminTableContainer} /> */}
      {/* <Route path={Routes.admin_places} component={VietosAdminLentelesContainer} /> */}

      <Route path="*" component={NoMatch} />
      <Route component={NoMatch} />
    </Switch>
    {props.children}
  </div>
);

export default App;
