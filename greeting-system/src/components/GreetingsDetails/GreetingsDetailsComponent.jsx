import React from "react";
import { Link } from "react-router-dom";

// import defaultImg from "../../images/default.png";

const GreetingsDetailsComponent = ({ greeting }) => {
  const { image, text, audioUrl, firstName, lastName } = greeting;

  // let imgSrc = image.substring(0, 5) === "https" ? image : defaultImg;
  return (
    <div>
      <div className="media">
        <img
          className="align-self-start mr-3"
          src={image}
          alt="paveiksliukas"
          style={{
            height: "25rem",
          }}
        />
        <div className="media-body mt-3">
          <h5 className="mt-0">
            {firstName} {lastName}
          </h5>
          <p>{text}</p>
          <p>Audio: {audioUrl}</p>
        </div>
      </div>
      <div className="row ml-5 mt-3">
        <Link to={"/"} className="btn btn-secondary">
          Back
        </Link>
      </div>
    </div>
  );
};

export default GreetingsDetailsComponent;
