import React, { Component } from "react";
import axios from "axios";
import baseUrl from "../../AppConfig";

import GreetingsDetailsComponent from "./GreetingsDetailsComponent";
import LoadingComponent from "../Loading/LoadingComponent";

class GreetingsDetailsContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      greeting: null,
    };
  }

  componentDidMount = () => {
    console.log(this.props.match.params.id);
    axios
      .get(`${baseUrl}/greetings/${this.props.match.params.id}`)
      .then((res) => this.setState({ greeting: res.data }))
      .catch((err) => console.log(err));
  };

  render() {
    if (this.state.greeting !== null) {
      return (
        <div className="container">
          <GreetingsDetailsComponent greeting={this.state.greeting} />
        </div>
      );
    } else {
      return <LoadingComponent />;
    }
  }
}

export default GreetingsDetailsContainer;
