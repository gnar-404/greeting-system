import React from "react";
import { Link } from "react-router-dom";

import defaultImg from "../../images/default.jpg";

var styles = {
  thumbnail: {
    maxWidth: "280px",
    textAlign: "center",
    marginLeft: "auto",
    marginRight: "auto",
  },
};

// dar prideti audioUrl tittle address logo!!!
const CardComponent = ({ id, text, image, firstName, lastName, date }) => {
  let imgSrc = image.substring(0, 5) === "https" ? image : defaultImg;
  return (
    <div className="container col-12 col-sm-12 col-md-6 col-lg-4 mb-4">
      <div className="m-3 card px-2 pt-3" style={styles.thumbnail}>
        <img src={imgSrc} style={styles.image} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">id: {id}</h5>

          <div>
            <p> {text} </p>
            <p>
              Name: {firstName} {lastName}{" "}
            </p>
            {/* <p>Date: {date} </p> */}
          </div>
          <Link to={`/greeting/${id}`} className="btn btn-info">
            Details here
          </Link>
        </div>
      </div>
    </div>
  );
};

export default CardComponent;
