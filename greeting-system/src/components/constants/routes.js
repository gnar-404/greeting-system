export const Routes = {
  // home: "/",

  all_greetings: "/",
  new_greeting: "/greetings",

  view_greeting: "/greetings/:id",
  edit_greeting: "/greetings/edit/:id",
  // greeting_id: "/greeting-system-backend/greeting/:id",
  // admin_greetings_new: "/greeting-system-backend/admin/greetings/new",
  // admin_greetings_id: "/greeting-system-backend/admin/greetings/id",
  // admin_greetings: "/greeting-system-backend/admin/greetings",
  // admin_places: "/greeting-system-backend/places",
  // places: "/greeting-system-backend/places",
};
