import React from "react";
import Loader from "react-loader-spinner";

const LoadingComponent = () => (
  <div className="d-flex justify-content-center mt-4">
    <div>
      <Loader type="BallTriangle" color="#00BFFF" height={80} width={80} />
    </div>
  </div>
);

export default LoadingComponent;
