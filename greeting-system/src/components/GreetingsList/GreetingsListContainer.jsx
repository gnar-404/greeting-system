import React, { Component } from "react";
import axios from "axios";
import baseUrl from "../../AppConfig";

import LoadingComponent from "../Loading/LoadingComponent";
// import GreetingsListComponent from "./GreetingsListComponent";
import CardComponent from "../CardComponent/CardComponent";

class GreetingsListContainer extends Component {
  constructor() {
    super();
    this.state = {
      greetings: [],
    };
  }

  componentDidMount = () => {
    console.log("Mounted...");
    axios
      .get(`${baseUrl}/greetings`)
      .then((response) => this.setState({ greetings: response.data }))
      .catch((err) => console.log(err));
  };

  render() {
    if (this.state.greetings.length > 0) {
      return (
        <div className="container">
          <div className="row">
            {this.state.greetings.map(({ id, ...otherProps }) => {
              return <CardComponent id={id} key={id} {...otherProps} />;
            })}
          </div>
        </div>
      );
    }

    // if (this.state.greetings.length > 0) {
    //   return <GreetingsListComponent greetings={this.state.greetings} />;
    // }
    else {
      return <LoadingComponent />;
    }
  }
}

export default GreetingsListContainer;
