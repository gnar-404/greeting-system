import React from "react";
import CardComponent from "../CardComponent/CardComponent";

const GreetingsListComponent = ({ greetings }) => (
  <div className="container">
    <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-4">
      {greetings.map(({ id, ...otherProps }) => {
        return <CardComponent key={id} id={id} {...otherProps} />;
      })}
    </div>
  </div>
);

export default GreetingsListComponent;
