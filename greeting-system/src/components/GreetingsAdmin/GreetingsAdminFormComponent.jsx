import React from "react";

import "../../styles/AdminForm.css";

const GreetingsAdminFormComponent = ({ handleChange, handleSubmit, ...otherProps }) => {
  const { text, image, date, firstName, audioUrl, lastName } = otherProps;
  //   const types = ["Live_building", "TV", "Internet"];
  return (
    <div>
      <form className="container" onSubmit={handleSubmit}>
        <div id="contact-form" className="form-container" data-form-container>
          <div className="row">
            <div className="form-title">
              <span>Greeting</span>
            </div>
          </div>

          <div className="input-container">
            {/* FIRST NAME.................................................... */}
            <div className="row">
              <span className="req-input">
                <span className="input-status" data-toggle="tooltip" data-placement="top" title="firstName">
                  {/* {" "} */}
                </span>
                <input
                  name="firstName"
                  type="text"
                  defaultValue={firstName}
                  onChange={handleChange}
                  placeholder="enter your name"
                  //   required="required"
                />
              </span>
            </div>

            {/* LAST NAME.................................................... */}
            <div className="row">
              <span className="req-input">
                <span className="input-status" data-toggle="tooltip" data-placement="top" title="lastName">
                  {/* {" "} */}
                </span>
                <input
                  name="lastName"
                  type="text"
                  defaultValue={lastName}
                  onChange={handleChange}
                  data-min-length="8"
                  placeholder="last name"
                  //   required="required"
                />
              </span>
            </div>

            {/* DATE.................................................... */}
            <div className="row">
              <span className="req-input">
                <span className="input-status" data-toggle="tooltip" data-placement="top" title="date"></span>
                <input
                  name="date"
                  type="text"
                  defaultValue={date}
                  onChange={handleChange}
                  data-min-length="8"
                  placeholder="DAR REIKIA SUTVARKYTI DATOS FORMATA"
                  //   required="required"
                />
              </span>
            </div>

            {/* AUDIO.................................................... */}
            <div className="row">
              <span className="req-input">
                <span className="input-status" data-toggle="tooltip" data-placement="top" title="audioUrl">
                  {/* {" "} */}
                </span>
                <input
                  name="audio"
                  type="text"
                  defaultValue={audioUrl}
                  onChange={handleChange}
                  data-min-length="8"
                  placeholder="audio url"
                  //   required="required"
                />
              </span>
            </div>

            {/* iMAGE.................................................... */}
            <div className="row">
              <span className="req-input">
                <span className="input-status" data-toggle="tooltip" data-placement="top" title="image">
                  {/* {" "} */}
                </span>
                <input
                  name="image"
                  type="text"
                  defaultValue={image}
                  onChange={handleChange}
                  data-min-length="8"
                  placeholder="Please Input image Url."
                  //   required="required"
                />
              </span>
            </div>

            {/* text.................................................... */}
            <div className="form-group">
              <label htmlFor="itemTekstas">Text:</label>
              <textarea
                name="text"
                defaultValue={text}
                onChange={handleChange}
                className="form-control"
                id="itemText"
                rows="3"></textarea>
            </div>

            <div className="row submit-row">
              <button type="submit" className="btn btn-block submit-form">
                Save
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
};

export default GreetingsAdminFormComponent;
