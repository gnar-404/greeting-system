import React from "react";
import { Link, Route } from "react-router-dom";
// import defaultImg from "../../images/default.png";

import ModalComponent from "../Modal/ModalComponent";
import "bootstrap/dist/js/bootstrap.bundle.min";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPencilAlt, faTrashAlt } from "@fortawesome/free-solid-svg-icons";

import { Routes } from "../../components/constants/routes";

const GreetingsAdminTableComponent = ({ greetings, deleteItem }) => {
  return greetings.map(({ id, image, firstName, lastName }, index) => {
    // const imgSrc = image.substring(0, 5) === "https" ? image : defaultImg;

    return (
      <tr key={id}>
        <th scope="row">{index + 1}</th>
        <td>
          <img src={image} className="card-img-top" style={{ width: 50, height: 50 }} alt="" />
        </td>
        <td>
          {firstName} {lastName}
        </td>
        {/* DAR PRIDETI CIA LAIKA */}
        <td>None</td>
        <td>
          <Link className="text-decoration-none mr-3" to={Routes.admin_greetings_id}>
            Edit <FontAwesomeIcon icon={faPencilAlt} />
          </Link>
          {/* KAS CIA PER STATIC BACKDROP???? */}
          <button className="btn btn-danger" data-toggle="modal" data-target={`#staticBackdrop${id}`} value={id}>
            Delete <FontAwesomeIcon icon={faTrashAlt} />
          </button>
        </td>
        <td>
          <ModalComponent itemId={id} deleteItem={deleteItem} />
        </td>
      </tr>
    );
  });
};

export default GreetingsAdminTableComponent;
