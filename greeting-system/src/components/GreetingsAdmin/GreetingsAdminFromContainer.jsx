import React, { Component } from "react";
import axios from "axios";
import { withRouter } from "react-router";
import baseUrl from "../../AppConfig";

import GreetingsAdminFormComponent from "./GreetingsAdminFormComponent";

import { Routes } from "../../components/constants/routes";

class GreetingsAdminFormContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      image: "",
      firstName: "",
      lastName: "",
      text: "",
      audioUrl: "",
      type: "",
      date: "",
      time: "",
    };
  }

  componentDidMount() {
    console.log("admin form container");
    if (this.props.match.params.id > 0) {
      axios
        .get(`${baseUrl}/greetings/${this.props.match.params.id}`)
        .then((res) => {
          const { id, date, image, firstName, lastName, audioUrl, text, time } = res.data;

          this.setState({
            id,
            date,
            image: image ? image : "",
            firstName,
            lastName: lastName ? lastName : "",
            audioUrl,
            text,
            time,
          });
        })
        .catch((err) => console.log(err));
    }
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { id, date, image, firstName, lastName, audioUrl, text, time } = this.state;

    if (id > 0) {
      axios
        .put(`${baseUrl}/greetings/${id}`, {
          id,
          image,
          text,
          firstName,
          audioUrl,
          date,
          lastName,
          time,
        })
        .then(() => this.props.history.push(Routes.all_greetings));
    } else {
      axios
        .post(`${baseUrl}/greetings`, {
          id,
          image,
          text,
          firstName,
          audioUrl,
          date,
          lastName,
          time,
        })
        .then(() => this.props.history.push(Routes.all_greetings));
    }
  };

  handleChange = (e) => {
    const { name, value } = e.target;

    this.setState({ [name]: value });
  };

  render() {
    return (
      <GreetingsAdminFormComponent handleSubmit={this.handleSubmit} handleChange={this.handleChange} {...this.state} />
    );
  }
}
export default withRouter(GreetingsAdminFormContainer);
