import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import axios from "axios";
import baseUrl from "../../AppConfig";

import { Routes } from "../../components/constants/routes";

import GreetingsAdminTableComponent from "./GreetingsAdminTableComponent";

class GreetingsAdminTableContainer extends Component {
  constructor() {
    super();
    this.state = {
      greetings: [],
    };
  }

  componentDidMount = () => {
    axios
      .get(`${baseUrl}/greetings`)
      .then((res) => this.setState({ greetings: res.data }))
      .catch((err) => console.log(err));
  };

  deleteItem = (e) => {
    e.preventDefault();
    axios
      .delete(`${baseUrl}/greetings/${e.target.value}`)
      .then(() => {
        axios.get(`${baseUrl}/greetings`).then((res) => this.setState({ greetings: res.data }));
      })
      .catch((err) => console.log(err));
  };

  render() {
    return (
      <div>
        <table className="table">
          <thead className="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Image</th>
              <th scope="col">Name</th>
              <th scope="col">Time</th>
              <th scope="col">Actions</th>
            </tr>
          </thead>
          {this.state.greetings.length > 0 && (
            <tbody>
              <GreetingsAdminTableComponent greetings={this.state.greetings} deleteItem={this.deleteItem} />
            </tbody>
          )}
        </table>

        <NavLink to={Routes.new_greeting} className="btn btn-dark mb-5 mt-3">
          Add
        </NavLink>
      </div>
    );
  }
}

export default GreetingsAdminTableContainer;
