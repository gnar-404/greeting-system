import React from "react";
import { NavLink } from "react-router-dom";

import "../../styles/Navigation.css";

import { Routes } from "../constants/routes";

import { TiTree } from "react-icons/ti";

const Navigation = () => (
  <nav className="navbar navbar-light navbar-expand-md" style={{ backgroundColor: "#282c34" }}>
    <div className="container">
      <ul className="navbarstyle nav navbar-nav">
        <div style={{ color: "white" }}>
          <TiTree size={32} />
        </div>
        <NavLink className="nav-link" exact to={Routes.all_greetings}>
          <div className="navco">Greetings</div>
        </NavLink>
        <NavLink className="nav-link" exact to={Routes.new_greeting}>
          <div className="navco">Add Greeting</div>
        </NavLink>
        {/* <NavLink className="nav-link" exact to={Routes.places}>
          <div className="navco">Places</div>
        </NavLink>
        <NavLink className="nav-link" to={Routes.admin_greetings}>
          <div className="navco">Greetings Admin</div>
        </NavLink>
        <NavLink className="nav-link" to={Routes.admin_places}>
          <div className="navco">Places Admin</div>
        </NavLink> */}
      </ul>
    </div>
  </nav>
);

export default Navigation;
